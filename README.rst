IaC - Packer + Terraform + Ansible
==================================

This repository contains 3 samples for vsphere:

  - How to generate an image by script - `packer <./packer>`_
  - How to manage your VMs - `terraform <./terraform>`_
  - How to config your VMs without using fixed IP - `ansible <./ansible>`_


Generate image
--------------

Use public cloud image
~~~~~~~~~~~~~~~~~~~~~~

Take ubuntu as an example, you can directly download the *ova* file from `Ubuntu Cloud Images <https://cloud-images.ubuntu.com/>`_.
And then upload it by *ovftool*

  .. code-block:: bash

    ovftool --acceptAllEulas --name=test-access-u1604 \
      --datastore=vsanDatastore --noSSLVerify=true --diskMode=thick \
      --vmFolder=RDLAB/Shihta.Kuan --network=DPortGroup_V1303 --overwrite \
      --prop:public-keys='ssh-rsa AAAAB3NzaC1yc2EA...' \
      ubuntu-16.04-server-cloudimg-amd64.ova \
      vi://YOURACCOUNT%40qctrd.com@192.168.1.13/QCTRD-Datacenter/host/QCTRD/Resources/RD%20lab

Notes: the url is encoded, *%40* means *@* and *%20* is a space.
You can directly set the image properties by :code:`--prop` as well.

Use packer
~~~~~~~~~~

You also can generate image by iso, the instructions on :code:`packer` as below:

  .. code-block:: bash

    PACKER_LOG=1 PACKER_LOG_PATH=log.txt PACKER_KEY_INTERVAL=10ms packer build build-from-iso.js

Notes: the default username and password of this VM are both "shihta"

Packer environment
~~~~~~~~~~~~~~~~~~

- Need VMWare workstation

- If you are going to install vmware workstation on VM, make sure the *Hardware virtualization* is enabled.

  .. image:: images/hardware_virtualization.png

- After you installed VMWare workstation, you can see the vmware information as below:

  .. code-block:: bash

    # vmware-installer -l
    Product Name         Product Version
    ==================== ====================
    vmware-workstation   14.1.2.8497320

    # vmware-installer -t
    Component Name                 Component Long Name                      Component Version
    ============================== ======================================== ====================
    vmware-installer               VMware Installer                                  2.1.0.7305623
    vmware-player-setup            VMware Player Setup                               14.1.2.8497320
    vmware-vmx                     VMware VMX                                        14.1.2.8497320
    vmware-network-editor          VMware Network Editor                             14.1.2.8497320
    vmware-network-editor-ui       VMware Network Editor User Interface              14.1.2.8497320
    vmware-tools-netware           VMware Tools for NetWare                          10.2.5.8497320
    vmware-tools-linuxPreGlibc25   VMware Tools for legacy Linux                     10.2.5.8497320
    vmware-tools-winPreVista       VMware Tools for Windows 2000, XP and Server 2003 10.2.5.8497320
    vmware-tools-winPre2k          VMware Tools for Windows 95, 98, Me and NT        10.2.5.8497320
    vmware-tools-freebsd           VMware Tools for FreeBSD                          10.2.5.8497320
    vmware-tools-windows           VMware Tools for Windows Vista or later           10.2.5.8497320
    vmware-tools-solaris           VMware Tools for Solaris                          10.2.5.8497320
    vmware-tools-linux             VMware Tools for Linux                            10.2.5.8497320
    vmware-player-app              VMware Player Application                         14.1.2.8497320
    vmware-workstation-server      VMware Workstation Server                         14.1.2.8497320
    vmware-ovftool                 VMware OVF Tool component for Linux               4.3.0.7948156
    vmware-vprobe                  VMware VProbes component for Linux                14.1.2.8497320
    vmware-workstation             VMware Workstation                                14.1.2.8497320

- If there is any library missing, you can try to fix them by adding following settings
  in :code:`/etc/ld.so.conf.d/vmware.conf` and run :code:`ldconfig`

  .. code-block:: bash

    /usr/lib/vmware/lib/libXft.so.2
    /usr/lib/vmware/lib/libXcomposite.so.1
    /usr/lib/vmware/lib/libXfixes.so.3
    /usr/lib/vmware/lib/libXext.so.6
    /usr/lib/vmware/lib/libXdamage.so.1
    /usr/lib/vmware/lib/libXdmcp.so.6
    /usr/lib/vmware/lib/libXcursor.so.1
    /usr/lib/vmware/lib/libXtst.so.6
    /usr/lib/vmware/lib/libXrender.so.1
    /usr/lib/vmware/lib/libXi.so.6
    /usr/lib/vmware/lib/libXau.so.6
    /usr/lib/vmware/lib/libX11.so.6
    /usr/lib/vmware/lib/libXinerama.so.1
    /usr/lib/vmware/lib/libXrandr.so.2

References
~~~~~~~~~~

- `Download VMware Workstation 12.5.9 Pro for Linux <https://my.vmware.com/web/vmware/details?downloadGroup=WKST-1259-LX&productId=524&rPId=20841>`_
- `Download Packer <https://www.packer.io/downloads.html>`_
- `VMware Builder (from ISO) <https://www.packer.io/docs/builders/vmware-iso.html>`_
- `vSphere Post-Processor <https://www.packer.io/docs/post-processors/vsphere.html>`_
- `vSphere Template Post-Processor <https://www.packer.io/docs/post-processors/vsphere-template.html>`_


Manage your VMs - Terraform
---------------------------

- Change your *ACCOUNT* and *PASSWORD* in :code:`pass.tf`.

- In this sample, terraform is going to create 3 VMs and put them on the folder "RDLAB/Shihta.Kuan".

- If you are using *ubuntu cloud image*, there are several properties can be used.
  You can use *ovftool* to check the usable properties:

  .. code-block:: bash

    $ ovftool ubuntu-16.04-server-cloudimg-amd64.ova
    ...
    Properties:
      Key:         instance-id
      Label:       A Unique Instance ID for this instance
      Type:        string
      Description: Specifies the instance id.  This is required and used to
                  determine if the machine should take "first boot" actions
      Value:       id-ovf

      Key:         hostname
      Type:        string
      Description: Specifies the hostname for the appliance
      Value:       ubuntuguest
    ...

- Use the *vapp* block to set those properties.
  Attention! you have to enable cdrom for *vapp*.
  You can refer to following part of :code:`main.tf`.

  .. code-block:: bash

    ...
    resource "vsphere_virtual_machine" "vm" {
      ...

      # cdrom {
      #   client_device = true
      # }
      # vapp {
      #   properties {
      #     hostname    = "st-hostname"
      #     public-keys = ""
      #   }
      # }
    }

Instructions
~~~~~~~~~~~~

  .. code-block:: bash

    terraform apply

References
~~~~~~~~~~

- `VMware vSphere Provider <https://www.terraform.io/docs/providers/vsphere/index.html>`_


Config your VMs
---------------

- This sample use vmware dynamic inventory to get VM IP,
  the original source is on `ansible/contrib/inventory/ <https://github.com/ansible/ansible/tree/devel/contrib/inventory>`_

- Change your *ACCOUNT* and *PASSWORD* in :code:`vmware.ini`.
  If you want to use cache, please use *absolute path* to set *cache_dir*.

- Verify your settings by this instruction:

  .. code-block:: bash

    ansible -i vmware.py -m raw -a "whoami" terraform-test-* -u shihta -k

{
  "builders": [
    {
      "type": "vmware-iso",
      "boot_command": [
        "<enter><wait><f6><esc><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        "/install/vmlinuz<wait>",
        " auto<wait>",
        " console-setup/ask_detect=false<wait>",
        " console-setup/layoutcode=us<wait>",
        " console-setup/modelcode=pc105<wait>",
        " debconf/frontend=noninteractive<wait>",
        " debian-installer=en_US<wait>",
        " fb=false<wait>",
        " initrd=/install/initrd.gz<wait>",
        " kbd-chooser/method=us<wait>",
        " keyboard-configuration/layout=USA<wait>",
        " keyboard-configuration/variant=USA<wait>",
        " locale=en_US<wait>",
        " netcfg/get_domain=vm<wait>",
        " netcfg/get_hostname=newhost<wait>",
        " grub-installer/bootdev=/dev/sda<wait>",
        " noapic<wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg",
        " -- <wait>",
        "<enter><wait>"
      ],
      "vnc_bind_address": "0.0.0.0",
      "vnc_disable_password": true,
      "boot_wait": "10s",
      "disk_size": 8192,
      "guest_os_type": "ubuntu-64",
      "headless": true,
      "http_directory": "http",
      "iso_url": "/root/ubuntu-16.04.4-server-amd64.iso",
      "iso_checksum": "6a7f31eb125a0b2908cf2333d7777c82",
      "iso_checksum_type": "md5",
      "shutdown_command": "echo 'shihta'|sudo -S shutdown -P now",
      "ssh_password": "shihta",
      "ssh_port": 22,
      "ssh_username": "shihta",
      "ssh_wait_timeout": "10000s"
    }
  ],
  "post-processors": [
    [
      {
        "type": "vsphere",
        "host": "192.168.1.13",
        "insecure": true,
        "username": "YOURACCOUNT@qctrd.com",
        "password": "YOURPASSWORD",
        "datacenter": "QCTRD-Datacenter",
        "datastore": "vsanDatastore",
        "vm_name": "ubuntu-16.04.4",
        "cluster": "QCTRD/Resources/RD%20lab",
        "vm_folder": "RDLAB/Shihta.Kuan",
        "vm_network": "DPortGroup_V1303",
        "overwrite": true
      },
      {
        "type": "vsphere-template",
        "host": "192.168.1.13",
        "insecure": true,
        "username": "YOURACCOUNT@qctrd.com",
        "password": "YOURPASSWORD",
        "datacenter": "QCTRD-Datacenter",
        "folder": "/RDLAB/Shihta.Kuan"
      }
    ]
  ]
}

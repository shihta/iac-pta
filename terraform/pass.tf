provider "vsphere" {
  user           = "YOURACCOUNT@qctrd.com"
  password       = "YOURPASSWORD"
  vsphere_server = "192.168.1.13"

  # If you have a self-signed cert
  allow_unverified_ssl = true
}


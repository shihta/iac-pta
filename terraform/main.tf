data "vsphere_datacenter" "dc" {
  name = "QCTRD-Datacenter"
}

data "vsphere_datastore" "datastore" {
  name = "vsanDatastore"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name          = "QCTRD/Resources/RD lab"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "DPortGroup_V1303"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "ubuntu-16.04.4-prebuild"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_host" "host_6" {
  name          = "192.168.1.54"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

output "output-host-6" {
  value = "${data.vsphere_host.host_6.id}"
}

output "output-dc" {
  value = "${data.vsphere_datacenter.dc.id}"
}

output "output-datastore" {
  value = "${data.vsphere_datastore.datastore.id}"
}

output "output-pool" {
  value = "${data.vsphere_resource_pool.pool.id}"
}

output "output-network" {
  value = "${data.vsphere_network.network.id}"
}

output "output-template" {
  value = "${data.vsphere_virtual_machine.template.id}"
}

resource "vsphere_virtual_machine" "vm" {
  count            = 3
  name             = "terraform-test-${count.index}"
  folder           = "RDLAB/Shihta.Kuan"
  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  host_system_id   = "${data.vsphere_host.host_6.id}"

  num_cpus = 4
  memory   = 8192

  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label            = "disk0"
    size             = "${data.vsphere_virtual_machine.template.disks.0.size}"
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"
  }

  # cdrom {
  #   client_device = true
  # }
  # vapp {
  #   properties {
  #     hostname    = "st-hostname"
  #     public-keys = ""
  #   }
  # }
}
